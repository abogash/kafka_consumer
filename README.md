To update bootstrap.servers or group or topic_name change data in src/main/resources/config.properties
Requirements:
- java > 1.7
- maven > 3.1

Alternative:
- run command: `java -jar com.bogash.kafka-1.0-SNAPSHOT-jar-with-dependencies.jar KafkaConsumerTest`
     -   note that in this case it will be runned with next params: bootstrap.servers=localhost:9092
                                                           group.id=test
                                                           topic_name=bogash
                                                           
                                                          
   ![](https://content.screencast.com/users/andrijbohash/folders/Snagit/media/784f3f2f-5ad6-406f-9a29-fe5afde31f32/04.03.2018-01.12.gif)
                                                       ddddddd