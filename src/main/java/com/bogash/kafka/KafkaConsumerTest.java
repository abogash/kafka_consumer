package com.bogash.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.Properties;
import java.util.ResourceBundle;

public class KafkaConsumerTest implements Runnable {

    KafkaConsumer<String, String> kafkaConsumer = null;
    private ResourceBundle propertiesFile = ResourceBundle.getBundle("config");

    static public void main(String args[]) {
     new KafkaConsumerTest().run();
    }

    public void run() {
        setup();
        startConsumer();
    }

    public void setup() {
        Properties props = new Properties();
        props.put("bootstrap.servers", propertiesFile.getString("bootstrap.servers"));
        props.put("group.id", propertiesFile.getString("group.id"));
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        kafkaConsumer = new KafkaConsumer<String, String>(props);
        kafkaConsumer.subscribe(Arrays.asList(propertiesFile.getString("topic_name")));
    }

    public void startConsumer() {
        System.out.println("Listening on topic=" + propertiesFile.getString("topic_name"));

        while (true) {
            ConsumerRecords<String, String> records = kafkaConsumer.poll(100);
            for (ConsumerRecord<String, String> record : records) {
                String key = record.key();
                String value = record.value();

                System.out.println((key != null ? "key=" + key + ", " : "")
                        + "message=" + value);
            }
        }

    }
}